#!/bin/bash

# get list of all v69 genes
mysql --host=ensembldb.ensembl.org --password="" --user="anonymous" homo_sapiens_core_69_37 -e "select gene_id, stable_id, version from gene;" > homo_sapiens_core_69_37.txt

# get lsit of all v86 genes
mysql --host=ensembldb.ensembl.org --password="" --user="anonymous" homo_sapiens_core_86_38 -e "select gene_id, stable_id, version from gene;" > homo_sapiens_core_86_38.txt

