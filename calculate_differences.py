# calculate_differences.py old_gene_version new_gene_version get_from_pickle debug=True initial_gene_id='ENS'



import sys, cPickle, os, errno
old_gene_ensembl_version =sys.argv[1]
new_gene_ensembl_version =sys.argv[2]
get_from_pickle = sys.argv[3]

try:
    debug = sys.argv[4]
    if debug == 'True':
        debug=True
    else:
        debug=False
except:
    debug = False


try:
    initial_gene_id = sys.argv[5]
except:
    initial_gene_id = 'ENS'


base_data_dir = "/home/rowlandm/tasks/t2621_ensembl_v69_v86/"

try:
    os.makedirs(base_data_dir)
except OSError as exception:
    if exception.errno != errno.EEXIST:
        raise


def get_full_list_of_genes(ensembl_version):
    dict_from_file = {}
    pickle_file_name = base_data_dir + ensembl_version+'_full_list_of_genes.pkl'
    if get_from_pickle !='no' :

        # Pickle dictionary using protocol 0.
        output = open(pickle_file_name, 'rb')
        result = cPickle.load(output)
    else:
        import pymysql.cursors

        connection = pymysql.connect(host="ensembldb.ensembl.org",
                                     user="anonymous",
                                     port=3306,
                                     password="",
                                     db=ensembl_version)

        with connection.cursor() as cursor:
            # Create a new record

            sql ="select gene_id, stable_id, version from gene;"

            cursor.execute(sql,)
            result = cursor.fetchall()


        output = open(pickle_file_name, 'wb')
        cPickle.dump(result, output)

    for temp_array in result:
        gene_id =  int(temp_array[0])
        stable_id = temp_array[1] # This is like a real ensembl ID
        version = int(temp_array[2])
        dict_from_file[stable_id] = {'gene_id':gene_id,'stable_id':stable_id,'version':version}

    return dict_from_file

def compare_dicts(original_dict,new_dict):

    summary_dict = {}
    summary_dict['same_id_diff_version'] = 0
    summary_dict['same_id_same_version'] = 0
    summary_dict['removed_id'] = 0
    summary_dict['added_id'] = 0
    same_dict= {}
    removed_list = []
    added_list = []
    for stable_id in original_dict:
        original_version = original_dict[stable_id]['version'] 
        if stable_id in new_dict:
            same_dict[stable_id] = {}

            new_version = new_dict[stable_id]['version'] 
            if new_version == original_version:
                same_dict[stable_id]['version'] = 'identical'
                summary_dict['same_id_same_version'] +=1
            else:
                same_dict[stable_id]['version'] = 'different'
                summary_dict['same_id_diff_version'] += 1
        else:
            removed_list.append(stable_id)
            summary_dict['removed_id'] += 1
                

    for stable_id in new_dict:
        if stable_id not in original_dict:
            added_list.append(stable_id)
            summary_dict['added_id'] += 1

    return [summary_dict,same_dict,removed_list,added_list]


def get_events_for_ensembl_ids(new_gene_ensembl_version,list_of_genes,sql):

    import pymysql.cursors
    connection = pymysql.connect(host="ensembldb.ensembl.org",
                                 user="anonymous",
                                 port=3306,
                                 password="",
                                 db=new_gene_ensembl_version)

    with connection.cursor() as cursor:
        # Create a new record

        format_strings = ','.join(['%s'] * len(list_of_genes))


        cursor.execute(sql % format_strings, tuple(list_of_genes))
        result = cursor.fetchall()
        return result


def get_data_for_removed_genes(get_from_pickle,list_of_genes_removed):

    remove_genes_pickle_file_name = base_data_dir + new_gene_ensembl_version+'_genes_removed.pkl'
    
    if get_from_pickle !='no' :

        # Pickle dictionary using protocol 0.
        output = open(remove_genes_pickle_file_name, 'rb')
        full_events_of_genes_removed = cPickle.load(output)

    else:
        #Note: this can handle all 8000+ sql queries!
        sql = "select * from stable_id_event where old_stable_id IN (%s) order by old_stable_id"
        full_events_of_genes_removed = get_events_for_ensembl_ids(new_gene_ensembl_version,list_of_genes_removed,sql)

        output = open(remove_genes_pickle_file_name, 'wb')
        cPickle.dump(full_events_of_genes_removed, output)


    return full_events_of_genes_removed

def get_data_for_added_genes(get_from_pickle,list_of_genes_added):

    add_genes_pickle_file_name = base_data_dir + new_gene_ensembl_version+'_genes_added.pkl'

    if get_from_pickle !='no' :


        # Pickle dictionary using protocol 0.
        output = open(add_genes_pickle_file_name, 'rb')
        full_events_of_genes_added = cPickle.load(output)

    else:
        #Note: this can handle all 8000+ sql queries!
        sql = "select * from stable_id_event where new_stable_id IN (%s) order by new_stable_id"
        full_events_of_genes_added = get_events_for_ensembl_ids(new_gene_ensembl_version,list_of_genes_added,sql)

        output = open(add_genes_pickle_file_name, 'wb')
        cPickle.dump(full_events_of_genes_added, output)


    return full_events_of_genes_added


def calculate_summary_for_removed_genes(full_events_of_genes_removed,list_of_genes_removed):
    summary_of_genes_removed_dict = {}
    dict_of_genes_latest_history = {}
    list_of_mapped_genes = []
    final_mapping_for_removed_genes = {}
    # old_stable_id, old_version, new_stable_id, new_version, mapping_version

    for row in full_events_of_genes_removed:
        old_state_id = row[0]


        old_version= row[1]
        new_state_id = row[2]
        new_state_version = row[3]
        mapping_session_id = row[4]

        if old_state_id not in dict_of_genes_latest_history:
            dict_of_genes_latest_history[old_state_id] = []

        if old_state_id != new_state_id:
            temp_dict = {}
            temp_dict['old_state_id'] = old_state_id
            temp_dict['old_version'] = old_version
            temp_dict['new_state_id'] = new_state_id
            temp_dict['new_state_version'] = new_state_version
            temp_dict['mapping_session_id'] = mapping_session_id
            dict_of_genes_latest_history[old_state_id].append(temp_dict)

    
    for old_state_id in dict_of_genes_latest_history:
        temp_row_list = dict_of_genes_latest_history[old_state_id] 
        number_of_mappings = len(temp_row_list)

        if number_of_mappings == 0:
            summary_calc = 'Removed - No rows found'

        if number_of_mappings == 1:
            new_state_id = temp_row_list[0]['new_state_id']
            if new_state_id is None:
                summary_calc = 'Removed - Terminated, no mapping (Single row with None new_state_id)'
                final_mapping_for_removed_genes[old_state_id] = []
            else:
                if initial_gene_id in new_state_id:
                    summary_calc = 'Removed - Not terminated?, mapping to another (Single row no None)'
                else:
                    summary_calc = 'Removed - Undefined (Single row)'

        if number_of_mappings >= 2:
            number_of_none = 0
            number_of_gene = 0
            number_of_undefined = 0
            list_of_genes_to_store = []
            for temp_row in  temp_row_list:
                new_state_id = temp_row['new_state_id']
                if new_state_id is None:
                    number_of_none += 1 
                else:
                    if initial_gene_id in new_state_id:
                        list_of_genes_to_store.append(new_state_id)
                        number_of_gene += 1
                    else:
                        number_of_undefined += 1
                        
            if number_of_none == 1 and number_of_gene == 1:
                summary_calc = 'Removed - Terminated, mapping to 1 other gene (1 None and 1 other row with gene for new_state_id)'
                final_mapping_for_removed_genes[old_state_id] = list_of_genes_to_store
            else:
                if number_of_none == 1 and number_of_gene > 1: 
                    temp_list = list(set(list_of_genes_to_store))
                    if len(temp_list) == 1:

                        summary_calc = 'Removed - Terminated, mapping to one other genes (1 None and 2+ other rows with single gene for new_state_id)'
                        final_mapping_for_removed_genes[old_state_id] = temp_list

                    else:
                        summary_calc = 'Removed - Terminated, mapping to multiple other genes (1 None and 2+ other rows with genes for new_state_id)'
                        final_mapping_for_removed_genes[old_state_id] = temp_list
                else:
                    if number_of_none == 2 and number_of_gene == 0:
                        summary_calc = 'Removed - Terminated (Two rows but all with new_state_id of None)'
                        final_mapping_for_removed_genes[old_state_id] = []
                    else:
                        summary_calc = 'Removed - Unknown'


        if summary_calc not in summary_of_genes_removed_dict:
            summary_of_genes_removed_dict[summary_calc] = 0

        summary_of_genes_removed_dict[summary_calc] += 1
 


    for summary_calc in summary_of_genes_removed_dict:
        if debug:
            print summary_calc + ':' + str(summary_of_genes_removed_dict[summary_calc])

    
    return summary_of_genes_removed_dict

def calculate_summary_for_added_genes(full_events_of_genes_added,list_of_genes_added):

    summary_of_genes_added_dict = {}
    dict_of_genes_latest_history = {}
    list_of_genes_mapped = []


    final_mapping_for_added_genes = {}

    for row in full_events_of_genes_added:
        old_state_id = row[0]
        old_version= row[1]
        new_state_id = row[2]
        new_state_version = row[3]
        mapping_session_id = row[4]


        if new_state_id not in dict_of_genes_latest_history:
            dict_of_genes_latest_history[new_state_id] = []

        if old_state_id != new_state_id:
            temp_dict = {}
            temp_dict['old_state_id'] = old_state_id
            temp_dict['old_version'] = old_version
            temp_dict['new_state_id'] = new_state_id
            temp_dict['new_state_version'] = new_state_version
            temp_dict['mapping_session_id'] = mapping_session_id
            dict_of_genes_latest_history[new_state_id].append(temp_dict)


    for new_state_id in dict_of_genes_latest_history:
        temp_row_list = dict_of_genes_latest_history[new_state_id] 
        number_of_mappings = len(temp_row_list)
        list_of_genes_to_store = []
        if number_of_mappings == 0:
            summary_calc = 'Added - Undefined (no rows found)'

        if number_of_mappings == 1:
            if temp_row_list[0]['old_state_id'] is None:
                summary_calc = 'Added - Brand New (1 row, with old_state_id None)'
                final_mapping_for_added_genes[new_state_id] = []
            else:
                summary_calc = 'Added - Single row - undefined'

        if number_of_mappings >= 2:
            number_of_none = 0
            number_of_gene = 0
            number_of_undefined = 0
            list_of_genes_to_store = []
            for temp_row in  temp_row_list:
                old_state_id = temp_row['old_state_id']
                if old_state_id is None:
                    number_of_none += 1 
                else:
                    if initial_gene_id in old_state_id:
                        number_of_gene += 1
                        list_of_genes_to_store.append(old_state_id)
                    else:
                        number_of_undefined += 1
                        
            if number_of_none == 1 and number_of_gene == 1:
                summary_calc = 'Added - Single Mapping, old terminated (2 rows, 1 with mapping, 1 with new_state_id of None)'
                final_mapping_for_added_genes[new_state_id] = list(set(list_of_genes_to_store))

            else:
                if number_of_none == 1 and number_of_gene > 1: 
                    temp_list = list(set(list_of_genes_to_store))
                    if len(temp_list) == 1:
                        summary_calc = 'Added - Single-mapping, old terminated (2+ rows, 1 with new_state_id of None, rest with single gene on multiple rows)'
                        final_mapping_for_added_genes[new_state_id] = temp_list
                    else:
                        summary_calc = 'Added - Multi-mapping, old terminated (2+ rows, 1 with new_state_id of None, rest with genes)'
                        final_mapping_for_added_genes[new_state_id] = temp_list
                else:
                    summary_calc = 'Added - Undefined'
        

        if summary_calc not in summary_of_genes_added_dict:
            summary_of_genes_added_dict[summary_calc] = 0


        summary_of_genes_added_dict[summary_calc] += 1
         

    for summary_calc in summary_of_genes_added_dict:
        if debug:
            print summary_calc + ':' + str(summary_of_genes_added_dict[summary_calc])


    return summary_of_genes_added_dict

def main():

    original_dict = get_full_list_of_genes(old_gene_ensembl_version)
    new_dict = get_full_list_of_genes(new_gene_ensembl_version)

    temp_result = compare_dicts(original_dict,new_dict)
    summary = temp_result[0]
    
    if debug:
        print summary

    list_of_genes_removed = temp_result[2]
    full_events_of_genes_removed = get_data_for_removed_genes(get_from_pickle,list_of_genes_removed)
    summary_genes_removed = calculate_summary_for_removed_genes(full_events_of_genes_removed,list_of_genes_removed)

    list_of_genes_added = temp_result[3]
    full_events_of_genes_added = get_data_for_added_genes(get_from_pickle,list_of_genes_added)
    summary_genes_added = calculate_summary_for_added_genes(full_events_of_genes_added,list_of_genes_added)
    
    return



main()
