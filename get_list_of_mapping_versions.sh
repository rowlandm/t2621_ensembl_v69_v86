#!/bin/bash



#NOTE: stable_id_event and mapping_session are added to each ensembl version.
# this means that we can simply use the "new" ensembl version database to search for the events that are changed.

LIST=`mysql --host=ensembldb.ensembl.org --password="" --user="anonymous" -e "show databases" | grep homo_sapiens_core_`

echo $LIST


for version in $LIST
do
    echo "$version";
    mysql --host=ensembldb.ensembl.org --password="" --user="anonymous" $version -e "select count(*) from stable_id_event;" 
    echo "-------------------------"
done
